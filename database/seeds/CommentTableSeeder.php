<?php

use Illuminate\Database\Seeder;

class CommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Comment::class, 10)->create()->each(function ($comment) {
            $comment->post()->associate(factory(App\Models\Post::class)->make());
        });
    }
}
