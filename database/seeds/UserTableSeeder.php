<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            'name' => 'Blog',
            'mobile' => '07035038924',
            'country' => 'Nigeria',
            'email' => 'gettosin4me@gmail.com',
            'password' => bcrypt('password'),
        ];
        factory(App\Models\User::class)->create($user);
        factory(App\Models\User::class, 9)->create()->each(function ($user) {
            $user->posts()->save(factory(App\Models\Post::class)->make());
        });
    }
}