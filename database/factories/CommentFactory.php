<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Comment::class, function (Faker $faker) {
    static $password;

    return [
        'user_id' => rand(1, 9),
        'post_id' => rand(1, 9),
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'comment' => $faker->text(200),
    ];
});
