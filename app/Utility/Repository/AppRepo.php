<?php
/**
 * Created by PhpStorm.
 * User: tosin
 * Date: 7/28/17
 * Time: 12:31 PM
 */

namespace App\Utility\Repository;


use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use RuntimeException;

class AppRepo
{
    public function __get($class)
    {
        switch ($class) {
            case 'comments':
                return new CommentRepo(new Comment());
                break;
            case 'posts':
                return new PostRepo(new Post());
                break;
            case 'users':
                return new UserRepo(new User());
                break;
        }

        throw new RuntimeException(
            'Trying to access undefined property '
            . __CLASS__ . '::$' . $class
        );
    }
}
