<?php
/**
 * Created by PhpStorm.
 * User: tosin
 * Date: 5/22/17
 * Time: 11:16 AM
 */

namespace App\Utility\Repository;


use App\Models\Post;

class PostRepo
{
    protected $model;

    public function __construct(Post $post)
    {
        $this->model = $post;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     *
     * return all candidate
     */
    public function all()
    {
        return $this->model->query()->get();
    }

    /**
     * @param $id
     *
     * Return a candidate using his primary key
     */
    public function findById($id)
    {
        return $this->model->query()->find($id);
    }

    /**
     * @param $title
     *
     * Return a candidate using his primary key
     */
    public function findByTitle($title)
    {
        return $this->model->query()->where('title', $title)->first();
    }

    /**
     * @param array $data
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function save(array $data)
    {
        return $this->model->save($data);
    }
}