<?php

namespace App\Utility\Transformers;

use App\Models\Comment;
use App\Utility\Transformers\PostTransformer;
use App\Utility\Transformers\UserTransformer;
use League\Fractal\ParamBag;
use League\Fractal\TransformerAbstract;

/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 6:42 PM
 */
class CommentTransformer extends TransformerAbstract
{
    /**
     * @var array
     */
    public $availableIncludes = [
        'user', 'post'
    ];

    /**
     * @param Post $post
     * @return array
     */
    public function transform(Comment $comment)
    {
        return [
            'id' => $comment->id,
            'comment' => $comment->comment,
            'name' => $comment->name,
            'email' => $comment->email,
            'created_at' => (string)$comment->created_at
        ];
    }

    public function includeUser(Comment $comment)
    {
        $user = $comment->user;
        
        return $this->item($user, new UserTransformer);
    }

    /**
     * @param Post $post
     * @return \League\Fractal\Resource\Item
     */
    public function includePost(Comment $comment)
    {
        $post = $comment->post;

        return $this->item($post, new PostTransformer);
    }
}
