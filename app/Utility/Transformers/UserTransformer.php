<?php

namespace App\Utility\Transformers;

use App\Models\User;
use App\Utility\Transformers\PostTransformer;
use App\Utility\Transformers\CommentTransformer;
use League\Fractal\ParamBag;
use League\Fractal\TransformerAbstract;

/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 6:42 PM
 */
class UserTransformer extends TransformerAbstract
{
    /**
     * @var array
     */
    public $availableIncludes = [
        'comments', 'post'
    ];

    /**
     * @param Post $post
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'id' => $user->id,
            'name' => $user->name,
            'mobile' => $user->mobile,
            'email' => $user->email,
            'country' => $user->country,
            'created_at' => (string)$user->created_at
        ];
    }

    public function includeComment(User $user)
    {
        $comments = $user->comments;

        return $this->collection($comments, new CommentTransformer);
    }

    /**
     * @param Post $post
     * @return \League\Fractal\Resource\Item
     */
    public function includePost(User $user)
    {
        $post = $user->posts;

        return $this->collection($post, new PostTransformer);
    }
}
