<?php

namespace App\Utility\Transformers;

use App\Models\Post;
use App\Utility\Transformers\UserTransformer;
use App\Utility\Transformers\CommentTransformer;
use League\Fractal\ParamBag;
use League\Fractal\TransformerAbstract;

/**
 * Created by PhpStorm.
 * User: Takomolafe
 * Date: 9/10/16
 * Time: 6:42 PM
 */
class PostTransformer extends TransformerAbstract
{
    /**
     * @var array
     */
    public $availableIncludes = [
        'comments', 'user'
    ];

    /**
     * @param Post $post
     * @return array
     */
    public function transform(Post $post)
    {
        return [
            'id' => $post->id,
            'title' => $post->title,
            'post' => $post->post,
            'comment' => count($post->comments),
            'created_at' => (string)$post->created_at
        ];
    }

    public function includeComments(Post $post)
    {
        $comments = $post->comments;
        
        return $this->collection($comments, new CommentTransformer);
    }

    /**
     * @param Post $post
     * @return \League\Fractal\Resource\Item
     */
    public function includeUser(Post $post)
    {
        $user = $post->user;

        return $this->item($user, new UserTransformer);
    }
}
