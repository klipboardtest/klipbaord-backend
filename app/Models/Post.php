<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    protected $fillable = ['user_id', 'post', 'title'];

    //User
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id')->withDefault([
            'name' => 'Guest',
        ]);;
    }

    //Comment
    public function comments()
    {
        return $this->hasMany(Comment::class, 'post_id', 'id');
    }
}
