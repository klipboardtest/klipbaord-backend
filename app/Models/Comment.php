<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';
    
    protected $fillable = ['user_id', 'post_id', 'comment', 'name', 'email'];

    //User
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    //Comment
    public function post()
    {
        return $this->belongsTo(User::class, 'post_id', 'id');
    }
}
