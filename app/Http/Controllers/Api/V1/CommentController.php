<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Utility\Repository\AppRepo;
use App\Utility\Transformers\CommentTransformer;
use Dingo\Api\Exception\StoreResourceFailedException;

class CommentController extends Controller
{
    protected $repo;

    public function __construct(AppRepo $appRepo)
    {
        $this->repo = $appRepo;
    }

    public function get()
    {
        $comments = $this->repo->comments->all();

        return $this->response->collection($comments, new CommentTransformer);
    }

    public function show($id)
    {
        $comment = $this->repo->comments->findById($id);

        if(!count($comment)) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException("Invalid Comment Id");
        }

        return $this->response->item($comment, new CommentTransformer);
    }

    public function create(Request $request, $postId)
    {
        $post = $this->repo->posts->findById($postId);

        $validator = \validator()->make($request->only(['comment', 'email', 'name']), [
            'comment' => ['required'],
            'email' => ['required', 'email'],
            'name' => ['required'],
        ]);

        if($validator->fails()) {
            throw new StoreResourceFailedException('Validation failded', $validator->messages());
        }

        $comment = $post->comments()->create($request->all());

        return $this->response->item($comment, new CommentTransformer);
    }

    public function update(Request $request, $id)
    {
        $comment = $this->repo->comments->findById($id);

        if(!count($comment)) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException("Invalid Comment Id");
        }

        $validator = \validator()->make($request->only(['comment']), [
            'comment' => ['sometimes', 'required'],
        ]);

        if($validator->fails()) {
            throw new StoreResourceFailedException('Validation failded', $validator->messages());
        }

        $comment->update($request->only(['comment']));

        return $this->response->item($comment, new CommentTransformer);
    }

    public function delete($id)
    {
        $comment = $this->repo->comments->findById($id);

        $comment->delete();

        return $this->response->noContent();
    }
}
