<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Utility\Repository\AppRepo;
use App\Utility\Transformers\PostTransformer;
use Dingo\Api\Exception\StoreResourceFailedException;

class PostController extends Controller
{
    protected $repo;

    public function __construct(AppRepo $appRepo)
    {
        $this->repo = $appRepo;
    }

    public function get()
    {
        $posts = $this->repo->posts->all();

        return $this->response->collection($posts, new PostTransformer);
    }

    public function show($id)
    {
        $post = $this->repo->posts->findById($id);

        if(!count($post)) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException("Invalid Post Id");
        }
        
        return $this->response->item($post, new PostTransformer);
    }

    public function getPostByTitle($title)
    {
        $post = $this->repo->posts->findByTitle($title);
        
        if(!count($post)) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException("Invalid Post Id");
        }
        
        return $this->response->item($post, new PostTransformer);
    }

    public function create(Request $request)
    {
        $user = $request->user();
        $validator = \validator()->make($request->only(['title', 'post']), [
            'title' => ['required'],
            'post' => ['required'],
        ]);

        if($validator->fails()) {
            throw new StoreResourceFailedException('Validation failded', $validator->messages());
        }

        $post = $user->posts()->create($request->all());

        return $this->response->item($post, new PostTransformer);
    }

    public function update(Request $request, $id)
    {
        $post = $this->repo->posts->findById($id);

        if(!count($post)) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException("Invalid Post Id");
        }

        $validator = \validator()->make($request->only(['title', 'post']), [
            'title' => ['sometimes', 'required'],
            'post' => ['sometimes', 'required'],
        ]);

        if($validator->fails()) {
            throw new StoreResourceFailedException('Validation failded', $validator->messages());
        }

        $post->update($request->only(['post', 'title']));

        return $this->response->item($post, new PostTransformer);
    }

    public function delete($id)
    {
        $post = $this->repo->posts->findById($id);

        return $this->response->noContent();
    }
}
