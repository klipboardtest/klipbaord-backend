<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Utility\Repository\AppRepo;
use App\Utility\Transformers\UserTransformer;

class UserController extends Controller
{
    public function __construct()
    {

    }

    public function get()
    {

    }

    public function show($id)
    {
        
    }

    public function create(Request $request)
    {
        
    }

    public function update(Request $request, $id)
    {
        
    }

    public function delete($id)
    {
        
    }
}
