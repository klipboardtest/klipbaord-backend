<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->get('posts', 'App\Http\Controllers\Api\V1\PostController@get');
    $api->get('posts/{post_id}', 'App\Http\Controllers\Api\V1\PostController@show');
    $api->get('posts/read/{title}', 'App\Http\Controllers\Api\V1\PostController@getPostByTitle');
    $api->group(['middleware' => ['auth:api'], 'namespace' => 'App\Http\Controllers\Api\V1'], function ($api) {
        $api->post('posts', 'PostController@create');
        $api->put('posts/{post_id}', 'PostController@update');
        $api->delete('posts/{post_id}', 'PostController@delete');
    });
});
