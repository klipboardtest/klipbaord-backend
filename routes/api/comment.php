<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->get('comments', 'App\Http\Controllers\Api\V1\CommentController@get');
    $api->get('comments/{post_id}', 'App\Http\Controllers\Api\V1\CommentController@show');
    $api->post('comments/posts/{post_id}', 'App\Http\Controllers\Api\V1\CommentController@create');
    $api->group(['middleware' => ['auth:api'], 'namespace' => 'App\Http\Controllers\Api\V1'], function ($api) {
        $api->put('comments/{post_id}', 'CommentController@update');
        $api->delete('comments/{post_id}', 'CommentController@delete');
    });
});
